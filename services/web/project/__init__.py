from flask import Flask, request, Response
from flask_sqlalchemy import SQLAlchemy
import datetime, os, psycopg2, string
from project.file import My_File, Connection

app = Flask(__name__)
app.config.from_object("project.config.Config")

db = SQLAlchemy(app)
 
def get_db_connection():
    conn = psycopg2.connect(host='db',
                            database='hello_flask_dev',
                            user="hello_flask",
                            password="hello_flask")
    return conn


class File(db.Model):
    __tablename__ = "files"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    location = db.Column(db.String(128))
    size= db.Column(db.String(128))
    type = db.Column(db.String(128))
    timestamp = db.Column(db.DateTime)

@app.route("/")
def home():
    return Response("Hello, World!", 200)

@app.route("/file", methods = ['GET', 'POST', 'DELETE'])
def file():
    if request.method == 'GET':
        if request.args.get('id'):
            get = My_File()
            print(get.get_file(request.args.get('id')))
            return Response("file",200)
    
    if request.method == 'POST': 
        if request.files:
            for k in request.files:
                file = request.files.get(k)               
                insert = My_File()
                insert.prepare_data(file)
                insert.insert_to_database()
            return Response("file_id: " + str(insert.id), 200)
        else:
            return Response("No file to upload!", 200)
         
    if request.method == 'DELETE':
        if request.args.get('id'):
            deleted = My_File()
            return_code = deleted.delete_from_database(request.args.get('id'))
            return Response("file deleted", 200)
        else:
            return Response("No file to delete", 200)
    
@app.route("/files", methods = ['GET'])
def files():
    conn = Connection()
    files = conn.select_files()
    print(files)
    return Response("files", 200)

if __name__ == "__main__":
    app.run(debug=True)