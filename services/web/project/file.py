import psycopg2
import os, datetime

class Connection():
    def __init__(self):
        self.conn = psycopg2.connect(host='db',
                            database='hello_flask_dev',
                            user="hello_flask",
                            password="hello_flask")
        self.cursor = self.conn.cursor()
        
    def select_files(self):
        sql= """SELECT * FROM files;"""
        files = self.cursor.execute(sql), 
        return files 

    def close_connecton(self):
        self.cursor.close()
        self.conn.close()

class My_File():
    def prepare_data(self, filestorage):
        self.type = filestorage.content_type
        self.name = filestorage.filename
        self.upload_time = datetime.datetime.now()
        self.location =  '/usr/src/app/files/'+ str(self.upload_time) + '_' + filestorage.filename
        filestorage.save(self.location)
        self.size = os.stat(self.location).st_size
        
    def insert_to_database(self):
        db_connect = Connection()
        sql = """INSERT INTO files(name,size,timestamp,type,location) VALUES(%s,%s,%s,%s,%s) RETURNING id;"""
        db_connect.cursor.execute(sql,(self.name, self.size, self.upload_time, self.type, self.location))
        self.id = db_connect.cursor.fetchone()[0]
        db_connect.conn.commit()
        db_connect.close_connecton()
        
    def delete_from_database(self, id):
        db_connect = Connection()
        sql = """DELETE FROM files WHERE files.id = %s;"""
        db_connect.cursor.execute(sql,(id,))
        db_connect.conn.commit()
        db_connect.close_connecton()
         
    def get_file(self, id):
        db_connect = Connection()
        sql = """SELECT * FROM files WHERE id = """ + id
        print(sql)
        file = db_connect.cursor.execute(sql)
        print(file)
        db_connect.close_connecton()
        return file